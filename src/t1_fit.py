import sys, os

if 'TOOLBOX_PATH' not in os.environ:
    print('TOOLBOX_PATH not set!')

sys.path.append(os.environ['TOOLBOX_PATH'] + '/python/')

from cfl import readcfl, writecfl
import numpy as np
from scipy.optimize import curve_fit
import warnings
warnings.filterwarnings('ignore')

def T1_func(x, a, b, c):
    return np.abs( a * ( 1 - np.exp( -1 / b * x + c ) ) )

if __name__ == "__main__":

    signal = np.abs(readcfl(sys.argv[1]).squeeze()).astype(np.float128) #dim = [x, y, time, slice]
    s = signal.shape
    
    # ms -> s
    tp = [float(t)*0.001 for t in sys.argv[2].split(" ")]

    # for each pixel we store the estimations for a,b in T1_func as well as the error for a,b
    results = np.zeros((s[0],s[1],4))

    # pixelwise curvefit of T1_func to the signal
    for i in range(0, s[0]):
        for j in range(0, s[1]):
            try:
                popt, pcov = curve_fit(T1_func, tp, signal[i,j,:], p0=(1, 1, 1))
                perr = np.sqrt(np.diag(pcov))
                results[i,j,0] = popt[0]
                results[i,j,1] = popt[1]
                results[i,j,2] = perr[0]
                results[i,j,3] = perr[1]
            except Exception:
                continue
    
    writecfl(sys.argv[3], results)
