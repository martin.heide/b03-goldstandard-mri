import sys, os

if 'TOOLBOX_PATH' not in os.environ:
    print('TOOLBOX_PATH not set!')

sys.path.append(os.environ['TOOLBOX_PATH'] + '/python/')

from cfl import readcfl, writecfl
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colormaps as cm
import matplotlib.gridspec as gp
import matplotlib.colors as col
from mpl_toolkits.axes_grid1 import make_axes_locatable
#import matplotlib.cm as cm

if __name__ == "__main__":

    # parameter map
    d = np.abs(readcfl(sys.argv[1]).squeeze())
    bounds = [sys.argv[2],sys.argv[3]]

    # tubes maps
#    masks = np.abs(readcfl(sys.argv[2]).squeeze())
    
    plt.style.use(['default'])
    cm_v = cm['viridis']
    cm_v.set_bad('white')

    fig = plt.figure()
    g = gp.GridSpec(1, 1, figure=fig)
    ax = fig.add_subplot(g[0])
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    ax.xaxis.set_ticks_position('none')
    ax.yaxis.set_ticks_position('none')

    im = ax.imshow(d, origin='lower', cmap=cm_v, vmax=bounds[1], vmin=bounds[0])

#    COLORS = ['darkorange', 'r', 'g', 'b', 'm', 'y', 'c', 'k', 'w', 'burlywood', 'lightcyan', 'mediumpurple', 'plum', 'slategrey']
#    for i in range(masks.shape[2]):
#        cm_r = col.ListedColormap(COLORS[i])
#        roi_tmp = np.ma.masked_equal(masks[:,:,i], 0)
#        im = ax.imshow(roi_tmp, origin='lower', cmap=cm_r, alpha=0.6)
    
#    im = ax.imshow(d, origin='lower', visible=False, cmap=cm_v, vmax=bounds[1], vmin=bounds[0])

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="6%", pad=0.05)
    cbar = plt.colorbar(im, cax=cax)
    FS=10
    cbar.set_label(sys.argv[4], fontsize=FS)
    cbar.ax.tick_params(labelsize=FS)
    
    plt.savefig(sys.argv[5])
